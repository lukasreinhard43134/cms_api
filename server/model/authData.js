const Connect = require('../service/dbPool');

const formatString = function(string) {
    return JSON.stringify(string);
};

const formatEmail = function(string) {
    return JSON.stringify(string);
};

const loginData = function(userName) {
    let connectionContext;
    return Connect()
        .then(function(connection) {
            connectionContext = connection;
            return connection.query('select * from tbl_users where username LIKE ' + formatString(userName) + ' ');
        })
        .then(function(results) {
            connectionContext.release();
            return Promise.resolve(results);
        });
};

const insertLastLogin = function(userName) {
    return Connect()
        .then(function(connection) {
            connectionContext = connection;
            return connection.query(
                'update tbl_users SET last_login= ' + formatEmail(new Date(new Date().setHours(new Date().getHours() + 7)).toISOString().replace(/T/, ' ').replace(/\..+/, '')) + ' ' +
                'where username = ' + formatString(userName));
        })
        .then(function(results) {
            connectionContext.release();
            return Promise.resolve(results);
        });

};

module.exports = {
    loginData,
    insertLastLogin
}