const Connect = require('../service/dbPool');

const formatString = function(string) {
    return JSON.stringify(string);
};

const getAll = function(usercode) {
    let connectionContext;
    return Connect()
        .then(function(connection) {
            connectionContext = connection;
            return connection.query(' select * from tbl_users ');
        })
        .then(function(results) {
            connectionContext.release();
            return Promise.resolve(results);
        });
};

const insertTest = function(username, role, password) {
    let connectionContext;
    return Connect()
        .then(function(connection) {
            connectionContext = connection;
            return connection.query('INSERT INTO tbl_users (username, role, password) VALUES (' + formatString(username) + ',' + formatString(role) + ',' + formatString(password) + ' );');
        })
        .then(function(results) {
            connectionContext.release();
            return Promise.resolve(results);
        });
};

module.exports = {
    getAll,
    insertTest
}