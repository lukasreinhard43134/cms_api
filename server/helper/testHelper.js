const TestModel = require('../model/testData')
const Logger = require('../helper/logger');

const getAll = () => {
    return new Promise((resolve, reject) => {
        try {
            const getData = TestModel.getAll();
            resolve(getData);
        } catch (err) {
            reject({ 'err.message': err });
        }
    });
};

const insertTest = (username, role, password) => {
    return new Promise(async(resolve, reject) => {
        try {
            const insertTest = await TestModel.insertTest(username, role, password);

            return resolve(insertTest);
        } catch (err) {
            Logger.write.log('error', 'Insert User: ' + err.message);
            return reject(err);
        }
    });
};

module.exports = {
    getAll,
    insertTest
};