const AuthData = require('../model/authData');
const Logger = require('../helper/logger');
const Bcrypt = require('bcrypt');

const _decryptPass = (password, hashPassword, userName) => {
    return new Promise((resolve, reject) => {
        try {
            Bcrypt.compare(password, hashPassword)
                .then((res, err) => {
                    if (!err) {
                        // AuthData.insertLastLogin(userName);
                        return resolve(res);
                    }
                    return reject({ 'err.message': err });
                })
        } catch (err) {
            Logger.write.log('error', 'Compare Password Hash: ', 'Helper' + err.message);
            reject({ 'err.message': err });
        }
    });
};

const loginHelper = (userName) => {
    return new Promise((resolve, reject) => {
        try {
            const getDataLogin = AuthData.loginData(userName);
            return resolve(getDataLogin);
            // const comparePass = _decryptPass(password, getHashPassword, userName);
            // if (comparePass) {
            //     return resolve({
            //         'statusCode': 'C0001',
            //         'response': 'Success',
            //         'message': 'Login succesfully'
            //     });
            // } else {
            //     return resolve({
            //         'statusCode': 'E0001',
            //         'response': 'Failed',
            //         'message': 'Wrong Password'
            //     });
            // }
        } catch (err) {
            Logger.write.log('error', 'Get Login: ', 'Helper' + err.message);
            reject({ 'err.message': err });
        }
    });
};

const insertLastLoginHelper = (userName) => {
    return new Promise((resolve, reject) => {
        try {
            const insertLastLogin = AuthData.insertLastLogin(userName);
            return resolve(insertLastLogin);
        } catch (err) {
            Logger.write.log('error', 'Insert Last Login : ', 'Helper' + err.message);
            reject({ 'err.message': err });
        }
    });
};

module.exports = {
    loginHelper,
    _decryptPass,
    insertLastLoginHelper
};