const _ = require('lodash');
const Joi = require('joi');
const Boom = require('boom');
const TestHelper = require('../helper/testHelper');

const getTest = async(request, h) => {
    const req = request.payload;
    try {
        const listAccounts = await TestHelper.getAll();
        return h.response(listAccounts);
    } catch (err) {
        throw Boom.notFound(err.message);
    }
};

const insertTest = async(request, h) => {
    const req = request.payload;
    try {
        const insertTest = await TestHelper.insertTest(req.username, req.role, req.password);
        return h.response(insertTest);
    } catch (err) {
        throw Boom.notFound('error', err);
    }
};

exports.plugin = {
    register: (server, options) => {
        options = _.extend({ basePath: '' }, options);

        server.route([{
            method: 'GET',
            path: '/test',
            handler: getTest,
            options: {
                description: 'Insert new Sales Quotation',
                tags: ['api', 'auth'],
                auth: false
            }
        }, {
            method: 'POST',
            path: options.basePath + '/insert-test',
            handler: insertTest,
            options: {
                description: 'Insert new test,',
                tags: ['api', 'test'],
                auth: false,
                validate: {
                    payload: {
                        username: Joi.string().required(),
                        role: Joi.string().required(),
                        password: Joi.string().required()
                    }
                }
            }
        }]);
    },

    'name': 'api-test'
};