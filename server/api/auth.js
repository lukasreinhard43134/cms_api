const _ = require('lodash');
const Joi = require('joi');
const Boom = require('boom');
const AuthHelper = require('../helper/authHelper');
const { insertLastLogin } = require('../model/authData');

const login = async(request, h) => {
    try {
        const req = request.payload;
        const getHashPassword = await AuthHelper.loginHelper(req.username);
        const response = {
            'username': getHashPassword[0].username,
            'role': getHashPassword[0].role
        }
        const checkHashPass = await AuthHelper._decryptPass(req.password, getHashPassword[0].password, req.username)
        if (checkHashPass) {
            const insertLastLogin = await AuthHelper.insertLastLoginHelper(req.username);
            return h.response(response);
        }
        return h.response({
            'statusCode': 'E0001',
            'response': 'Failed',
            'message': 'Wrong input email or password'
        });


    } catch (error) {
        throw Boom.badRequest(error.errorMessage);
    }
};

exports.plugin = {
    register: (server, options) => {
        options = _.extend({ basePath: '' }, options);

        server.route([{
            method: 'POST',
            path: options.basePath + '/login',
            handler: login,
            options: {
                description: 'login,',
                tags: ['api', 'auth'],
                auth: false,
                validate: {
                    payload: {
                        username: Joi.string().required(),
                        password: Joi.string().required()
                    }
                }
            }
        }]);
    },

    'name': 'api-auth'
};