const Config = require('./config');
const Confidence = require('confidence');

const criteria = {
    env: process.env.NODE_ENV
};

const manifest = {
    server: {
        port: Config.get('/port/api'),
        routes: {
            cors: true,
            security: true
        }
    },
    register: {
        plugins: [
            { plugin: 'inert' },
            { plugin: 'vision' },
            {
                plugin: 'good',
                options: Config.get('/logging')
            },
            { plugin: './server/api/test', routes: { prefix: '/api/test' } },
            { plugin: './server/api/auth', routes: { prefix: '/api/auth' } },
            { plugin: './server/plugin/exceptionHandler', routes: {} },
            { plugin: './server/plugin/responseHandler', routes: {} }
        ]
    }
};

module.exports = manifest;

var store = new Confidence.Store(manifest);

exports.get = function(key) {
    return store.get(key, criteria);
};

exports.meta = function(key) {
    return store.meta(key, criteria);
};