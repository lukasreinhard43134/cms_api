const Glue = require('glue');
const Inert = require('inert');
const Manifest = require('./manifest');
const Logger = require('./server/helper/logger');

const options = {
    relativeTo: __dirname
};

if (process.env.NODE_ENV !== 'production') {
    Manifest.register.plugins.push({
        plugin: 'blipp',
        // options: {
        //   showAuth: true
        // }
    });
}

// Show bad promises
process.on('unhandledRejection', (reason, p) => {
    console.log(p, reason);
    Logger.write.log('error', 'Unhandled Rejection at: ' + JSON.stringify(p), 'reason: ' + JSON.stringify(reason));
});

const startServer = async() => {
    const server = await Glue.compose(
        Manifest,
        options
    );

    await server.register(Inert);
    await server.start();
};

startServer();