const Confidence = require('confidence');

const criteria = {
    env: process.env.NODE_ENV || 'local'
};

const config = {
    $meta: 'This file is configuration of middleware',
    projectName: 'danon-middleware',
    port: {
        api: {
            $filter: 'env',
            dev: 8102,
            // dev: 8084,
            preprod: 2213,
            $default: process.env.NODE_PORT || 5000
        }
    },
    routes: {
        cors: {
            origin: ['*'],
        }
    },
    logging: {
        reporters: {
            myConsoleReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ fatal: '*', error: '*', info: '*', log: '*', debug: '*', response: '*' }]
            }, {
                module: 'good-console'
            }, 'stdout'],
            myFileReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ fatal: '*', error: '*', info: '*', log: '*', debug: '*' }]
            }, {
                module: 'good-squeeze',
                name: 'SafeJson'
            }, {
                module: 'good-file',
                args: [{
                    $filter: 'env',
                    local: './log/web-api-local.log',
                    dev: './log/web-api-dev.log',
                    preprod: './log/web-api-preprod.log',
                    $default: './log/web-api-local.log'
                }]
            }]
        }
    },
    dbConnection: {
        $filter: 'env',
        local: {
            host: '127.0.0.1',
            user: 'lukas',
            password: 'lakukan123#',
            database: 'cms_db',
            port: 3306,
            connectionLimit: 50,
            thread_handling: 'pool-of-threads',
            thread_pool_size: 32,
            thread_pool_stall_limit: 300,
            thread_pool_oversubscribe: 10
        },
        dev: {
            host: '',
            user: '',
            password: '',
            database: '',
            port: 8084,
            connectionLimit: 50,
            thread_handling: 'pool-of-threads',
            thread_pool_size: 32,
            thread_pool_stall_limit: 300,
            thread_pool_oversubscribe: 10
        },
        preprod: {
            host: '',
            user: '',
            password: '',
            database: '',
            port: 3306,
            connectionLimit: 50,
            thread_handling: 'pool-of-threads',
            thread_pool_size: 32,
            thread_pool_stall_limit: 300,
            thread_pool_oversubscribe: 10
        },
        $default: {
            host: '127.0.0.1',
            user: 'lukas',
            password: 'lakukan123#',
            database: 'cms_db',
            port: 3306,
            connectionLimit: 50,
            thread_handling: 'pool-of-threads',
            thread_pool_size: 32,
            thread_pool_stall_limit: 300,
            thread_pool_oversubscribe: 10
        }
    }
};

var store = new Confidence.Store(config);

exports.get = (key) => {
    return store.get(key, criteria);
};

exports.meta = (key) => {
    return store.meta(key, criteria);
};